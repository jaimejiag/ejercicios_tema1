# README #

### ¿De qué trata este repositorio? ###

Este repositorio esta dedicado a una app en Android.
Contiene una vista principal que actúa como menú, cinco botones como componentes y cada botón se encarga de lanzar una actividad con uno de los cincos ejercicios propuestos.

**Ejercicio 1**

Convierte una cantidad en dólares a euros y viceversa.

**Ejercicio 2**

Convierte centímetros en pulgadas.

**Ejercicio 3**

Indicas un sitio web y te envía a él.

**Ejercicio 4**

Contador de cafés que has consumido con una cuenta atrás programada.

**Ejercicio 5**

Explicado con más detalle a continuación.

### Ejercicio 5 ###

El ejercicio trata de una simulación de verificación de términos por parte de un usuario. 
Consta de dos activity. Una recoge el nombre del usuario y te permite observar el resultado elegido por el usuario en la segunda activity.
La segunda activity muestra un mensaje con el nombre del usuario introducido en la primera vista, y es donde puedes acepta o rechazar las condiciones.

**Primera activity**

![captura_1.png](https://bitbucket.org/repo/rqMLM5/images/821359470-captura_1.png)

Contiene un EditText donde deberá el usuario poner su nombre, un Button que lanza la segunda actividad, un TextView donde se podrá observar el resultado elegido por el usuario en la segunda activity y un Button 'salir'.

**Segunda activity**

![captura_2.png](https://bitbucket.org/repo/rqMLM5/images/398193599-captura_2.png)

Contiene un TextView donde aparecerá siempre esa frase pero saludará con el nombre del usuario que se introdujo en el activity anterior, más dos Button donde aceptas o rechazas las condiciones, según el Button pulsado aparecerá un resulta u otro en la primera activity.