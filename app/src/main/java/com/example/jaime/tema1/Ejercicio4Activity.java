package com.example.jaime.tema1;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Ejercicio4Activity extends AppCompatActivity implements View.OnClickListener{
    TextView cafes, tiempo; //Los minutos del tiempo
    Button menos, mas, comenzar;
    Contador contador;
    MyCountDownTimer miContadorDescendiente;
    public static final int PAUSA = 5;
    public static final int INTERVALO = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);

        cafes = (TextView) findViewById(R.id.txv_contador);
        tiempo = (TextView) findViewById(R.id.txv_tiempo);
        menos = (Button) findViewById(R.id.btn_menos);
        mas = (Button) findViewById(R.id.btn_mas);
        comenzar = (Button) findViewById(R.id.btn_comenzar);

        menos.setOnClickListener(this);
        mas.setOnClickListener(this);
        comenzar.setOnClickListener(this);

        contador = new Contador(PAUSA, 0);
    }

    @Override
    public void onClick(View v) {
        if (v == menos)
            tiempo.setText(contador.disminuirTiempo() + ":00");
        if (v == mas)
            tiempo.setText(contador.aumentarTiempo() + ":00");
        if (v == comenzar){
            miContadorDescendiente = new MyCountDownTimer(contador.getTiempo() * 60 * 1000, INTERVALO); //Pasa a segundos y luego a milisegundos.
            miContadorDescendiente.start();
            menos.setEnabled(false);
            mas.setEnabled(false);
            comenzar.setEnabled(false);
        }
    }

    public  class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int minutos, segundos;

            minutos = (int) ((millisUntilFinished / 1000) / 60);
            segundos = (int) ((millisUntilFinished /1000) % 60);
            tiempo.setText(minutos + ":" + segundos);
        }

        @Override
        public void onFinish() {
            tiempo.setText("Pausa terminada");
            cafes.setText(contador.aumentarCafes());
            menos.setEnabled(true);
            mas.setEnabled(true);
            comenzar.setEnabled(true);
        }
    }
}
