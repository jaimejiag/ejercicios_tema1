package com.example.jaime.tema1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Ejercicio5Activity extends AppCompatActivity {
    EditText mEdt_nombre;
    TextView mTxv_resultado;
    Button mBtn_verificar;
    Button mBtn_salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio5);

        final Intent intent = new Intent(this, CondicionesActivity.class);

        mEdt_nombre = (EditText) findViewById(R.id.edt_nombre);
        mTxv_resultado = (TextView) findViewById(R.id.txv_resultado);

        mBtn_verificar = (Button) findViewById(R.id.btn_verificar);
        mBtn_verificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("nombre", mEdt_nombre.getText().toString());
                intent.putExtras(bundle);
                startActivityForResult(intent, 1234);
            }
        });

        mBtn_salir = (Button) findViewById(R.id.btn_salir);
        mBtn_salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String resultado = "";

        if (requestCode == 1234 && resultCode == RESULT_OK){
            mTxv_resultado.setText("Resultado: aceptado");
        }
        else if (requestCode == 1234 && resultCode == RESULT_CANCELED)
            mTxv_resultado.setText("Resultado: rechazado");
    }
}
