package com.example.jaime.tema1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu_Activity extends AppCompatActivity implements View.OnClickListener{

    Button btnEj1;
    Button btnEj2;
    Button btnEj3;
    Button btnEj4;
    Button btnEj5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_);

        btnEj1 = (Button) findViewById(R.id.btn_ej1);
        btnEj2 = (Button) findViewById(R.id.btn_ej2);
        btnEj3 = (Button) findViewById(R.id.btn_ej3);
        btnEj4 = (Button) findViewById(R.id.btn_ej4);
        btnEj5 = (Button) findViewById(R.id.btn_ej5);

        btnEj1.setOnClickListener(this);
        btnEj2.setOnClickListener(this);
        btnEj3.setOnClickListener(this);
        btnEj4.setOnClickListener(this);
        btnEj5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        if (v == btnEj1){
            intent = new Intent(this, Ejercicio1Activity.class);
            startActivity(intent);
        }
        if (v == btnEj2) {
            intent = new Intent(this, Ejercicio2Activity.class);
            startActivity(intent);
        }
        if (v == btnEj3) {
            intent = new Intent(this, Ejercicio3Activity.class);
            startActivity(intent);
        }
        if (v == btnEj4) {
            intent = new Intent(this, Ejercicio4Activity.class);
            startActivity(intent);
        }
        if (v == btnEj5) {
            intent = new Intent(this, Ejercicio5Activity.class);
            startActivity(intent);
        }
    }
}
