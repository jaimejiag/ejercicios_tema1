package com.example.jaime.tema1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class Ejercicio3Activity extends AppCompatActivity {

    EditText edtUrl;
    Button btnIr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);

        edtUrl = (EditText) findViewById(R.id.edt_url);

        btnIr = (Button) findViewById(R.id.btn_ir);
        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtUrl.getText().toString()))
                    openWebPage(edtUrl.getText().toString());
            }
        });
    }

    public void openWebPage (String url){
        WebView webview = new WebView(this);
        webview.loadUrl(url);
    }
}
