package com.example.jaime.tema1;

public class Contador {
    int tiempo;
    int cafes;

    public Contador (){
        this.tiempo = 5;
        this.cafes = 0;
    }

    public Contador (int t, int c){
        this.tiempo = t;
        this.cafes = c;
    }

    public String disminuirTiempo() {
        this.tiempo -= 1;
        if(this.tiempo < 1)
            tiempo = 1;

        return  String.valueOf(this.tiempo);
    }

    public String aumentarTiempo(){
        tiempo += 1;
        return  String.valueOf(this.tiempo);
    }

    public  String aumentarCafes(){
        cafes += 1;
        return String.valueOf(cafes);
    }

    public int getTiempo() {
        return tiempo;
    }

    public int getCafes() {
        return cafes;
    }
}
