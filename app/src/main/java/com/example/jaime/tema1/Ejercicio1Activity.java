package com.example.jaime.tema1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class Ejercicio1Activity extends AppCompatActivity implements View.OnClickListener{

    private static final double CAMBIO = 1.12;  //1€ -> 1.12$
    EditText edtDolares;
    EditText edtEuros;
    RadioButton rdbDolarEuro;
    RadioButton rdbEuroDolar;
    Button btnConvertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);

        edtDolares = (EditText) findViewById(R.id.edt_dolares);
        edtEuros = (EditText) findViewById(R.id.edt_euros);
        rdbDolarEuro = (RadioButton) findViewById(R.id.rdb_dolarEuro);
        rdbEuroDolar = (RadioButton) findViewById(R.id.rdb_euroDolar);
        btnConvertir = (Button) findViewById(R.id.btn_convertir);
        btnConvertir.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String resultado;

        if (rdbDolarEuro.isChecked() == true){
            if (!TextUtils.isEmpty(edtDolares.getText().toString())){
                resultado = convertirAEuros(edtDolares.getText().toString());
                edtEuros.setText(resultado);
            }
        } else if (rdbEuroDolar.isChecked() == true) {
            if (!TextUtils.isEmpty(edtEuros.getText().toString())) {
                resultado = convertirADolares(edtEuros.getText().toString());
                edtDolares.setText(resultado);
            }
        }
    }

    public String convertirADolares (String cantidad){
        double valor = Double.parseDouble(cantidad) * CAMBIO;
        return String.format("%.2f", valor);
    }

    public String convertirAEuros (String cantidad){
        double valor = Double.parseDouble(cantidad) / CAMBIO;
        return String.format("%.2f", valor);
    }
}
