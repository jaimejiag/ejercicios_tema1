package com.example.jaime.tema1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Ejercicio2Activity extends AppCompatActivity {

    public static final double PULGADAS = 0.39; // 1cm = 0.39 pulgadas
    EditText edtCentimetros;
    TextView txvPulgadas;
    Button btnConvertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);

        edtCentimetros = (EditText) findViewById(R.id.edt_centimetros);
        txvPulgadas = (TextView) findViewById(R.id.txv_pulgadas);

        btnConvertir = (Button) findViewById(R.id.btn_convertir);
        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtCentimetros.getText().toString())) {
                    double resultado = Double.parseDouble(edtCentimetros.getText().toString()) * PULGADAS;
                    txvPulgadas.setText(String.format("%.2f", resultado));
                }
            }
        });
    }
}
