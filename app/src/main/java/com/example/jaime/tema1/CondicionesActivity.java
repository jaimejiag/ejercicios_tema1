package com.example.jaime.tema1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CondicionesActivity extends AppCompatActivity implements View.OnClickListener{
    TextView mTxv_aceptar;
    Button mBtn_aceptar;
    Button mBtn_rechazar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condiciones);

        mTxv_aceptar = (TextView) findViewById(R.id.txv_aceptar);
        Bundle bundle = getIntent().getExtras();
        mTxv_aceptar.setText("Hola " + bundle.getString("nombre") + " ¿Aceptas las condiciones?");

        mBtn_aceptar = (Button) findViewById(R.id.btn_aceptar);
        mBtn_aceptar.setOnClickListener(this);

        mBtn_rechazar = (Button) findViewById(R.id.btn_rechazar);
        mBtn_rechazar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mBtn_aceptar){
            setResult(RESULT_OK);
        }
        else{
            setResult(RESULT_CANCELED);
        }

        finish();
    }
}
